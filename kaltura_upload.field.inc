<?php

/**
 * Implements hook_field_widget_info().
 *
 * @return type
 */
function kaltura_upload_field_widget_info() {
  return array(
    'entryid_kaltura_upload' => array(
      'label' => t('Kaltura Upload'),
      'field types' => array('field_kaltura_entryid'),
      'settings' => array(
        'kaltura_upload_user' => '',
        'kaltura_upload_ui_conf' => '',
        'kaltura_upload_user_admin' => 0
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    )
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function kaltura_upload_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $form = array();
  if ($widget['type'] == 'entryid_kaltura_upload') {
    $defaults = field_info_widget_settings($widget['type']);
    $settings = array_merge($defaults, $widget['settings']);
    $form['kaltura_upload_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Kaltura Upload User'),
      '#required' => TRUE,
      '#description' => t('The user to use for uploading to Kaltura.'),
      '#default_value' => $settings['kaltura_upload_user'],
      '#weight' => 200,
    );
    $form['kaltura_upload_ui_conf'] = array(
      '#type' => 'textfield',
      '#title' => t('Kaltura Upload UI Configuration'),
      '#required' => TRUE,
      '#description' => t('The upload UI configuration.'),
      '#default_value' => $settings['kaltura_upload_ui_conf'],
      '#weight' => 201,
    );
    $form['kaltura_upload_user_admin'] = array(
      '#type' => 'checkbox',
      '#title' => t('Kaltura user is administrator'),
      '#description' => t('Check this if the upload user is a Kaltrua administrator.'),
      '#default_value' => $settings['kaltura_upload_user_admin'],
      '#weight' => 202,
    );
  }
  return $form;
}

/**
 * Implements hook_field_widget_form().
 *
 * @return type
 */
function kaltura_upload_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  // Get the item.
  $item = !empty($items[$delta]) ? $items[$delta] : null;
  $entryId = !empty($item['entryid']) ? $item['entryid'] : '';

  // Add the entry item.
  $element['entry'] = array(
    '#theme' => 'kaltura_upload_entry',
    '#entryid' => $entryId,
  );

  // Add just the upload widget.
  $element['upload'] = array(
    '#theme' => 'kaltura_upload',
    '#entryid' => $entryId,
  );

  require_once $GLOBALS['_kaltura_client_folder'] . 'KalturaClient.php';
  $partnerId = variable_get('kaltura_partner_id', '');
  $kuser = $instance['widget']['settings']['kaltura_upload_user'];
  $kadmin = !empty($instance['widget']['settings']['kaltura_upload_user_admin']);
  $kusertype = empty($kadmin) ? KalturaSessionType::USER : KalturaSessionType::ADMIN;
  $secret = empty($kadmin) ? variable_get('kaltura_secret', '') : variable_get('kaltura_admin_secret', '');

  // Get the configuration and client.
  $config = new KalturaConfiguration($partnerId);
  $client = new KalturaClient($config);
  $ks = $client->session->start($secret, $kuser, $kusertype);

  $element['upload']['#kaltura'] = array(
    '#uid' => $kuser,
    '#ks' => $ks,
    '#partnerId' => $partnerId,
    '#uiConfId' => $instance['widget']['settings']['kaltura_upload_ui_conf'],
  );

  // Get the base Id.
  $baseId = 'kaltura-upload-' . $langcode . '-' . $delta;

  // Add the entry id.
  $element['entryid'] = array(
    '#type' => 'textfield',
    '#default_value' => $entryId,
    '#prefix' => '<div style="display:none;">',
    '#suffix' => '</div>',
  );

  $element['mediatype'] = array(
    '#type' => 'textfield',
    '#default_value' => !empty($item['mediatype']) ? $item['mediatype'] : '',
    '#prefix' => '<div style="display:none;">',
    '#suffix' => '</div>',
  );

  return $element;
}
