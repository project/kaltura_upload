(function($) {
  $(function() {

    // Make sure we keep them from navigating away...
    var setConfirmUnload = function(on) {
      window.onbeforeunload = on ? function() {
        return "You're still uploading! Are you sure you want to leave this page?";
      } : null;
    }

    // Make sure we have settings
    if (Drupal.settings.kaltura_upload) {

      // Iterate through the upload instances, although there should only be one.
      $.each(Drupal.settings.kaltura_upload, function(id, settings) {

        // The base id of this widget.
        var baseId = id.replace(/-upload$/, '');

        // Get all of the upload DOM elements.
        var uploader = null;
        var context = $('#' + id);
        var uploaderId = id + '-uploader';
        var title = '';
        var text = $('.kaltura-upload-text', context);
        var button = $('#kaltura-upload-button', context);
        var entryid = $('#' + baseId + '-entryid');
        var mediatype = $('#' + baseId + '-mediatype');
        var progress = $('.kaltura-upload-progress span.meter', context);
        var mediaEntry = $('#' + baseId + '-entry');
        var image = $('.kaltura-upload-image', mediaEntry);
        var name = $('.kaltura-upload-title', mediaEntry);

        $('.kaltura-upload-remove').click(function(event) {
          event.preventDefault();
          entryid.val('');
          mediatype.val('');
          mediaEntry.hide('slow');
          context.show('slow');
        });

        // Get an instance name from the id.
        var instance = id.replace(/[^A-Za-z0-9]/g, '');

        // Setup the delegator.
        Drupal.kaltura_upload = Drupal.kaltura_upload || {};
        Drupal.kaltura_upload[instance] = {
          readyHandler: function() {
            uploader = document.getElementById(uploaderId);
            text.empty();
            button.val('Choose File');
          },
          selectHandler: function() {
            var bytesTotal = uploader.getTotalSize();
            var error = uploader.getError();
            if (error) {
              text.text('Error: ' + error);
            }
            else if (!bytesTotal) {
              text.text('Error: Unknown file size.');
            }
            else {
              setConfirmUnload(true);
              uploader.upload();
            }
          },
          allUploadsCompleteHandler: function() {
            var error = uploader.getError();
            if (error) {
              text.text('Error: ' + error);
            }
            else {
              text.text('Processing...');

              // Get the node title.
              var nodeTitle = $('#edit-title').val();
              title = nodeTitle ? nodeTitle : title;
              uploader.setTitle(title, 0, 0);
              uploader.addEntries();
            }
          },
          progressHandler: function(args) {

            // Get the file and update the percentage.
            var file = args[2], percent = 0, val = '';
            if (file.bytesTotal) {

              // Calculate the percent complete.
              title = file.title;
              percent = Math.round((file.bytesLoaded / file.bytesTotal) * 100);
              progress.css('width', percent + '%');
              val += Math.round(file.bytesTotal / 1024 / 1024, 2) + 'Mb';
              text.text('Uploading... ' + percent + '% of ' + val);
            }
            else {

              // Set an error display...
              text.text('Error: Unknown file size.');
            }
          },
          uiConfErrorHandler: function() {
            text.text('Error: UI Configuration Error.');
          },
          entriesAddedHandler: function(entries) {
            var error = uploader.getError();
            if (error) {
              text.text('Error: ' + error);
            }
            else {
              if(entries && entries[0]) {
                var entry = entries[0];
                entryid.val(entry.entryId);
                mediatype.val(entry.mediaTypeCode);
                text.text('Upload Success!');
                image.attr('src', entry.thumbnailUrl);
                name.text(entry.title);
                context.hide('slow');
                mediaEntry.show('slow');
                setConfirmUnload(false);
              }
            }
          }
        };

        // Create the flash variables.
        var flashVars = {
          entryId: settings.entryId,
          uid: settings.uid,
          partnerId: settings.partnerId,
          ks: settings.ks,
          jsDelegate: 'Drupal.kaltura_upload.' + instance
        };

        // Create the swfupload object.
        swfobject.embedSWF(
          "http://www.kaltura.com/kupload/ui_conf_id/" + settings.uiConfId,
          uploaderId,
          button.width() + 'px',
          button.height() + 'px',
          "9.0.0",
          "expressInstall.swf",
          flashVars,
          {
            allowScriptAccess: "always",
            allowNetworking: "all",
            wmode: "transparent"
          },
          {
            id: uploaderId,
            name: uploaderId,

          }
        );
      });
    }
  });
})(jQuery);
