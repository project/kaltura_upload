<?php

include_once('kaltura_upload.field.inc');

/**
 * Implementation of hook_library().
 */
function kaltura_upload_library() {
  $path = drupal_get_path('module', 'kaltura_upload');
  $libraries['kaltura_upload'] = array(
    'title' => 'jQuery Upload',
    'website' => 'https://github.com/blueimp/jQuery-File-Upload',
    'version' => '8.8.5',
    'dependencies' => array(
      array('system', 'ui.widget')
    ),
    'js' => array(
      'http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' => array('type' => 'external', 'group' => JS_LIBRARY),
      $path . '/js/kaltura-upload.js' => array('group' => JS_LIBRARY)
    ),
    'css' => array(
      $path . '/css/kaltura-upload.css' => array(
        'type' => 'file',
        'media' => 'screen'
      ),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_permission().
 */
function kaltura_upload_permission() {
  return array(
    'administer kaltura uploads' => array(
      'title' => t('Administer Kaltura Uploads'),
      'description' => t('Perform administration on Kaltura Uploaded values.'),
    )
  );
}

/**
 * Implementation of hook_theme().
 * @return type
 */
function kaltura_upload_theme() {
  $themes = array();
  $themes['kaltura_upload'] = array(
    'render element' => 'element'
  );
  $themes['kaltura_upload_entry'] = array(
    'render element' => 'element'
  );
  return $themes;
}

/**
 * Provided an entryId, this returns the media item.
 *
 * @param type $entryId
 * @return type
 */
function kaltura_upload_get_media($entryId) {
  $k_helpers = new KalturaHelpers();
  $kaltura_client = $k_helpers->getKalturaClient(true);
  return $kaltura_client->baseEntry->get($entryId);
}

function theme_kaltura_upload_entry($variables) {
  $element = $variables['element'];
  $elementid = $element['#id'];
  $entryid = $element['#entryid'];

  $style = '';
  if ($entryid) {
    $media = kaltura_upload_get_media($entryid);
  }
  else {
    $style = 'style="display:none;"';
    $media = new stdClass();
    $media->thumbnailUrl = '';
    $media->name = '';
  }

  return '
  <div id="' . $elementid . '" class="kaltura-upload-entry row" ' . $style . '>
    <a href="#" class="kaltura-upload-remove"><img src="/misc/message-24-error.png" /></a>
    <img class="kaltura-upload-image" src="' . $media->thumbnailUrl . '" />
    <span class="kaltura-upload-title">' . $media->name . '</span>
  </div>';
}

/**
 * Theme function for the kaltura_upload.
 */
function theme_kaltura_upload($variables) {
  $element = $variables['element'];
  $uploadId = $element['#id'];

  // The settings for our upload widget.
  $settings = json_encode(array(
    'entryId' => '-1',
    'uid' => $element['#kaltura']['#uid'],
    'partnerId' => $element['#kaltura']['#partnerId'],
    'ks' => $element['#kaltura']['#ks'],
    'uiConfId' => $element['#kaltura']['#uiConfId'],
  ));

    // Add these settings.
  drupal_add_js("
    Drupal.settings.kaltura_upload = Drupal.settings.kaltura_upload || {};
    Drupal.settings.kaltura_upload['{$uploadId}'] = {$settings};
  ", 'inline');

  // Add the upload widget library.
  drupal_add_library('kaltura_upload', 'kaltura_upload');

  $style = '';
  if (!empty($element['#entryid'])) {
    $style = 'style="display:none"';
  }

  // Output the widget.
  $output =  '<div id="' . $uploadId . '" class="row kaltura-upload" ' . $style . '>';
  $output .=    '<div class="large-2 columns kaltura-upload-input">';
  $output .=      '<input type="button" id="kaltura-upload-button" class="button kaltura-upload-button" value="Loading..." />';
  $output .=      '<span id="' . $uploadId . '-uploader"></span>';
  $output .=    '</div>';
  $output .=    '<div class=" kaltura-upload-progress large-10 columns progress"><span class="meter">';
  $output .=      '<span class="kaltura-upload-text">Initializing...</span>';
  $output .=    '</span></div>';
  $output .=  '</div>';
  return $output;
}
